<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/controllers/test/MyToast.php');
include APPPATH.'libraries/teste/Celular.php';

class CelularTest extends MyToast{
	
	function __construct() {
		parent::__construct('CelularTest');
    }

    function test_celular_tem_marca(){
        $cel = new Celular('Philco');
        $marca = $cel->getMarca();
        $this->_assert_equals('Philco', $marca, "Valor esperado: Philco, valor recebido: $marca");
    }

 
    function test_marca_nao_pode_ser_vazia(){
        $cel = new Celular('');
        $marca = $cel->getMarca();
        $this->_assert_equals(-1, $marca, "Valor esperado: -1, valor recebido: $marca");
    }

    function test_preco_inicia_nulo(){
        $cel = new Celular('xxx');
        $preco = $cel->getPreco();
        $this->_assert_equals_strict(0, $preco, "Esperado 321, calculado $preco");
    }

    
    function test_preco_dinamico(){
        $cel = new Celular('xxx');
        $cel->setPreco(321);
        $preco = $cel->getPreco();
        $this->_assert_equals(321, $preco, "Esperado 321, calculado $preco");

        $preco = $cel->setPreco(123);
        $preco = $cel->getPreco();
        $this->_assert_equals(123, $preco, "Esperado 123, calculado $preco");
    }

    function test_preco_nao_pode_ser_negativo(){
        $cel = new Celular('xxx');
        $cel->setPreco(-123);
        $preco = $cel->getPreco();
        $this->_assert_equals_strict(0, $preco, "Esperado 0, calculado $preco");
    }

    function test_celular_usa_ate_dois_chips(){
        $cel = new Celular('zzz');
        $num = $cel->numeroDeChips();
        $this->_assert_equals_strict(0, $num, "Esperado 0, recebido $num");
        
        $cel->adicionaChip('96574569');
        $num = $cel->numeroDeChips();
        $this->_assert_equals(1, $num, "Esperado 1, recebido $num");
        $chip = $cel->getChip(0);
        $this->_assert_equals('96574569', $chip, "Esperado '96574569', recebido $chip");
        
        $cel->adicionaChip('484654121');
        $num = $cel->numeroDeChips();
        $this->_assert_equals(2, $num, "Esperado 2, recebido $num");
        $chip = $cel->getChip(1);
        $this->_assert_equals('484654121', $chip, "Esperado '484654121', recebido $chip");
        
        $cel->adicionaChip('846413213');
        $num = $cel->numeroDeChips();
        $this->_assert_equals(2, $num, "Esperado 2, recebido $num");
    }

    function test_usar_rede_3G(){
        $cel = new Celular('aaaa');
        $b = $cel->usa3G();
        $this->_assert_true($b, 'A rede 3g não está disponível');
    }

    
}