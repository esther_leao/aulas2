<?php
defined('BASEPATH') OR exit('No direct script acces allowed');
//include_once APPPATH.'libraries/teoria/Pessoa.php';
include_once APPPATH.'libraries/teoria/Aluno.php';
include_once APPPATH.'libraries/teoria/Professor.php';
include_once APPPATH.'libraries/teoria/Funcionario.php';
include_once APPPATH.'libraries/teoria/Biblioteca.php';

class TeoriaModel extends CI_Model{
    public function heranca(){
        $p1 = new Pessoa('Maria', 25);
        $p2 = new Pessoa('José', 28);
        var_dump($p1); echo '<br>';
        var_dump($p2); echo '<br><br>';
        echo "<br> A idade de ".$p1->getNome()." é ".$p1->getIdade()."";
        echo "<br> A idade de ".$p2->getNome()." é ".$p2->getIdade()."";


        $p1->setIdade(34);
        $p2->setIdade(38);
        echo"<br>Depois de executar a função setIdade<br>";
        echo "<br>A idade de ".$p1->getNome(). "é:".$p1->getIdade();
        echo "<br>A idade de ".$p2->getNome(). " é :".$p2->getIdade();

        echo"<br><br>Depois de executar a função getPressao<br>";
        echo "<br>A Pressao de  ".$p1->getNome(). " é:".$p1->getPressao();
        echo "<br>A Pressao de ".$p2->getNome(). " é :".$p2->getPressao();

        echo"<br><br>Depois de executar a função andar<br>";
        echo "<br>A Posição de  ".$p1->getNome(). " é:".$p1->andar();
        echo "<br>A Posição de ".$p2->getNome(). " é :".$p2->andar();
        echo'<br>';


        $a1 = new Aluno('Einstein', 22, 'Turma de sexta');
        $a1->setTurma('LP2-  Sexta Feira');
        //var_dump($a1);
        

        echo"<br>Metodos do aluno<br>";
        echo "<br>A idade de  ".$a1->getNome(). " é:".$a1->getIdade();
        echo "<br>A Posição de ".$a1->getNome(). " é :".$a1->andar();
        echo "<br>A Pressao de ".$a1->getNome(). " é :".$a1->getPressao();
        echo "<br>A turma do ".$a1->getNome(). " é : ".$a1->getTurma();
        echo'<br>';
    }

    public function polimorfismo(){
        $livros = array(2, 8, 6, 3, 9, 5);
        $aluno = new Aluno('Maria', 25, 'ADS');
        $professor = new Professor('João', 45, 'IFSP');
        $func = new Funcionario('José', 35, 'Carpintaria');

        $bib = new Biblioteca();
        $bib->emprestimo($livros, $aluno);
        $bib->emprestimo($livros, $professor);
        $bib->emprestimo($livros, $func);

        $bib->devolucao($livros, $aluno);
        $bib->devolucao($livros, $professor);
        $bib->devolucao($livros, $func);
        
    }
}


?>