<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH.'libraries/padroes/observer/Estoque.php';

class PadroesModel extends CI_Model{
    
    public function observer(){
        $usuarios = array('Ana', 'Bia', 'Eva');

        $this->load->library('padroes/observer/Loja');
        $this->load->library('padroes/observer/Sms', $usuarios);
        $this->load->library('padroes/observer/Push', $usuarios);
        //$this->load->library('padroes/observer/Estoque', '17/05/2019');
        $estoque = new Estoque('17/05/2019');

        $this->loja->addListener($this->sms);
        $this->loja->addListener($this->push);
        $this->loja->addListener($estoque);

        $this->loja->realiza_venda();

        $this->loja->removeListener($this->push);
        echo "<br>depois de algum tempo... <br>";
        $this->loja->realiza_venda();

        


    }

}