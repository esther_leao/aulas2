<div class="container mt-3">


    <div class="card">
        <div class="card-header"><h4>Endereço</h4></div>
        <div class="card-body">
                

    <div class="form-row">

        <div class="col-md-4">

        <div class="md-form form-group">
            <input type="text" value="<?=set_value('tipo_logradouro') ?>" class="form-control" name="tipo_logradouro" id="tipo_logradouro" placeholder="Avenida, Rua, etc">
            <label for="tipo_logradouro">Tipo do logradouro</label>
        </div>
        </div>

        <div class="col-md-8">

        <div class="md-form form-group">
            <input type="text" value="<?=set_value('nome_logradouro') ?>" class="form-control" name="nome_logradouro" id="nome_logradouro" placeholder="Tiradentes, Paulista">
            <label for="nome_logradouro">Nome do logradouro</label>
        </div>
        </div>

    </div>

    <div class="row">

        <div class="col-md-2">
            <div class="md-form form-group">
                <input type="text" value="<?=set_value('numero') ?>" class="form-control" name="numero" id="numero" placeholder="Complemento">
                <label for="numero">Numero</label>
            </div>
        </div>

        <div class="col-md-4">

            <div class="md-form form-group">
                <input type="text" value="<?=set_value('complemento') ?>" class="form-control" name="complemento" id="complemento" placeholder="Numero">
                <label for="complemento">Complemento</label>
            </div>
        </div>

        <div class="col-md-4">

            <div class="md-form form-group">
                <input type="text" value="<?=set_value('cep') ?>" class="form-control" name="cep" id="cep" placeholder="00.000-000">
                <label for="cep">CEP</label>
            </div>
        </div>

    </div>

    <div class="form-row">

        <div class="col-md-6">

        <div class="md-form form-group">
            <input type="text" value="<?=set_value('cidade') ?>" class="form-control" name="cidade" id="cidade" placeholder="São Paulo, Guarulhos, etc">
            <label for="cidade">Cidade</label>
        </div>
        </div>


        <div class="col-md-6">

        <div class="md-form form-group">
            <input type="text"  value="<?=set_value('estado') ?> "class="form-control" name="estado" id="estado" placeholder="São Paulo, Rio de Janeiro">
            <label for="estado">Estado</label>
        </div>
        </div>

    </div>



    </div>
    </div>

</div>