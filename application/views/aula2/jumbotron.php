<!-- Jumbotron -->
<div class="jumbotron text-center blue-grey lighten-5">

  <!-- Title -->
  <h2 class="card-title h2"><?= $title ?></h2>

  <!-- Subtitle -->
  <p class="indigo-text my-4 font-weight-bold"><?= $subtitle ?></p>

  <!-- Grid row -->
  <div class="row d-flex justify-content-center">

    <!-- Grid column -->
    <div class="col-xl-7 pb-2">

      <p class="card-text"><?= $desc ?></p>

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->

  <hr class="my-4 pb-2">

  <button class="btn blue-gradient btn-rounded"><?= $b1 ?> <i class="far fa-gem ml-1"></i></button>
  <button class="btn btn-indigo btn-rounded"><?= $b2 ?> <i class="fas fa-download ml-1"></i></button>

</div>
<!-- Jumbotron -->