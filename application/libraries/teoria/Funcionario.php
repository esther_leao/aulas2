<?php
include_once 'Pessoa.php';
class Funcionario Extends Pessoa{
    private $setor;
    private $funcao;

    function __construct($nome, $idade, $funcao){
        parent::__construct($nome, $idade);
        $this->funcao = $funcao;
    }

    public function trabalhar(){
        echo 'Finge que está trabalhando mas na realidade está assistindo naruto';
    }

    public function limiteLivros(){
        return 4;
    }

    public function prazoDeEntrega(){
        return 14;
    }

    public function atraso(){
        return 10;
    }
}
?>