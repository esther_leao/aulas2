<?php

include_once 'Pessoa.php';

class Aluno extends Pessoa{
    private $turma;
    private $curso;

    function __construct($nome, $idade, $turma){
        parent::__construct($nome,$idade);
        $this->turma = $turma;
    }

    public function setTurma($turma){
        $this->turma = $turma;
    }

    public function getTurma(){
        return $this->turma;
    }

    public function estudar(){
        echo 'Passar horas procrastinando<br>';
    }

    public function pesquisar(){
        echo 'Pegar o primeiro resultado que acha no google<br>';
    }

    public function limiteLivros(){
        return 7;
    }

    public function prazoDeEntrega(){
        return 21;
    }

    public function atraso(){
        return 4;
    }
}