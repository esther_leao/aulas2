<?php

abstract class Pessoa{
    
    private $nome;
    private $idade;
    private $pressao;

    function __construct($nome, $idade){
        $this->idade = $idade;
        $this->nome = $nome;
    } 

    public function getNome(){
        return $this->nome;
    }

    public function getIdade(){
        return $this->idade;
    }

    public function setIdade($idade){
        $this->idade = $idade;
    }

    public function getPressao(){
        $s = rand(5, 25);
        $d = rand(3, 15);
        return "$s x $d";
    }

    public function andar(){
        $x = rand(-30,30);
        $y = rand(-30,30);
        return "($x, $y)";
    }

    //força as classes filhas a criarem sua própria versão deste método
    public abstract function limiteLivros();
    public abstract function prazoDeEntrega();
    public abstract function atraso();
}

?>