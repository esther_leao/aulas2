<?php
include_once 'Pessoa.php';

class Professor extends Pessoa{
    private $escola;
    private $curso = array();

    function __construct($nome, $idade, $escola){
        parent::__construct($nome, $idade);
        $this->escola = $escola;
    }

    public function addCurso($curso){
        $this->curso[] = $curso;
    }

    public function lecionar(){
        echo 'Fala sobre  a vida dele, religião, dieta, time de futebol, etc<br>';
    }

    public function pesquisar(){
        echo 'Como liga o projetor??<br>';
    }

    public function limiteLivros(){
        return 10;
    }

    public function prazoDeEntrega(){
        return 30;
    }

    public function atraso(){
        return 6;
    }

}

?>