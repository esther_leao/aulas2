<?php

include_once 'VendasListener.php';
include_once 'Mensagem.php';

class Push extends Mensagem implements VendasListener{
    function __construct(array $listaUsuarios){
        parent::__construct($listaUsuarios);
    }

    protected function envia($usuario){
        echo "nOTIFICAÇÃO enviada para o celular do cliente $usuario </br>";
    }

    public function compra_realizada($produto){
    }
}