<?php

include_once 'VendasListener.php';
include_once 'Mensagem.php';

class Sms extends Mensagem implements VendasListener{
        
    function __construct(array $listaUsuarios){
        parent::__construct($listaUsuarios);
    }

    protected function envia($usuario){
        echo "SMS enviado com sucesso<br>";
    }
    
    //Implementação vazia é necessária ou pode ser necessária quando não existe a 
    //necesssidade de implementar todos os métodos de uma interface
    public function compra_realizada($produto){}
}