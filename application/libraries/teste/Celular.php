<?php

class Celular{

    private $marca;
    private $preco = 0;
    private $chips = array();

    public function __construct($marca){
        $this->marca = $marca;
    }
   /**
     * Informa a marca do celular.
     * @return string: marca do celular
     * @return int -1: caso não tenha marca
     * 
     */
    public function getMarca(){
        if(strlen($this->marca))
            return $this->marca;
        return -1;
    }

    public function getPreco(){
        return $this->preco;
    }

    public function setPreco($preco){
        $this->preco = $preco > 0 ? $preco : 0;

    }

    public function adicionaChip($num){
        if($this->numeroDeChips() < 2)
            $this->chips[] = $num;
    }

    public function numeroDeChips(){
        return sizeof($this->chips);
    }

    public function getChip($index){
        return $this->chips[$index];
    }

    public function usa3G(){
        return true;
    }
}

?>